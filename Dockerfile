# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
# copy jar into image
ADD /target/eazey-notification-0.0.1-SNAPSHOT.jar eazeynotification.jar
# run application with this command line 
CMD ["/usr/bin/java", "-jar", "eazeynotification.jar"]

