# Eazey Notification Service

### Firebase Account Setup

1. Login to Firebase Console
1. Create a New Project 
1. Generate and Download Private Key Credentials JSON
    1. Go into the project, and go to "Project Settings"
    1. Go to "Service Accounts" tab
    1. Click on "Create service account" (if not done before)
    1. Click on "Generate new private key" button
    1. In the popup that appears, click on "Generate Key" button
    1. The key json file will be generated and downloaded
    
### Configuration

1. Copy the downloaded json file to Application's resources 
    1. Copy the file to src/main/resources/firebase directory
1. Set the path to this json file in application-local.yml (for local profile), 
and in appropriate application configuration file for other profiles.

Example: 
~~~
app:
    firebase-configuration-file: firebase/demoapp-firebase-adminsdk.json    
~~~
    
    
### API Reference

This service is configured with OpenAPI 3 documentation and comes with Swagger UI.
To view the API documentation open http://<hostname>:<port>/swagger-ui.html

Example:
[http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)


### Usage

#### Push Notification to User

API to push a notification to user is:

~~~
POST http://<host>:<port>/users/<user_id>/notify
~~~
Example:
~~~

POST http://localhost:8080/users/john@gmail.com/notify

Request Body (Json)
{
    "title":"Order Received",
    "message":"We have received your order.",
    "data": {
        "orderId":"ORDER12345"
    }
}
~~~
`title` field will be used as title of the notification, and `message` is the main content of the notification.
Field `data` is optional and can be used to pass some additional data to the mobile app to take some action.


#### Push Notification to Group

API to push notification to a group:
~~~
POST http://<host>:<port>/groups/<group_id>/notify
~~~
Example:
~~~
{
	"title":"Discount Offers",
	"message":"Offers specially for you! Shop now!",
	"data": {
		"discount":"20"
	},
	"push_type":"TOPIC"
}
~~~
`title` and `message` field usage is same as above.

`push_type` field value can be either **TOPIC** or **BATCH**.
The difference is in the way the notifications are delivered.

In *TOPIC* push type only the topic-name is sent to FCM, and FCM uses publish/subscribe model to delivery 
notification to all subscribed devices, where as
in *BATCH* type, we need to pass list of device-Ids (from Group's users list) to send notification to, FCM will immediately push
notification to all users. Limitation with BATCH type is that maximum of 500 device Ids can be passed per request.

##### Batch Push Response

When we use *BATCH* type to push notification to group, the API response will return the statistics as below:
~~~
{
    "total_devices": 3,
    "total_success": 2,
    "total_failure": 1
}
~~~

