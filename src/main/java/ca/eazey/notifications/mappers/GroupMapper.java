package ca.eazey.notifications.mappers;

import org.springframework.stereotype.Service;

import ca.eazey.notifications.dto.GroupNotificationResponse;
import ca.eazey.notifications.model.firebase.BatchPushResponse;

@Service
public class GroupMapper {

    public GroupNotificationResponse modelToDto(BatchPushResponse batchPushResponse) {
        GroupNotificationResponse groupNotificationResponse = new GroupNotificationResponse();
        groupNotificationResponse.setTotalDevices(batchPushResponse.getTotalDevices());
        groupNotificationResponse.setTotalFailure(batchPushResponse.getFailureCount());
        groupNotificationResponse.setTotalSuccess(batchPushResponse.getSuccessCount());
        return  groupNotificationResponse;
    }

}
