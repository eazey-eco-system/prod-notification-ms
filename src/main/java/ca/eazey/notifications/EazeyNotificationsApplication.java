package ca.eazey.notifications;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EazeyNotificationsApplication {

	public static void main(String[] args) {
		SpringApplication.run(EazeyNotificationsApplication.class, args);
	}

}
