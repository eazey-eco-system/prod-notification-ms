package ca.eazey.notifications.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;

public class CustomException extends Exception {

    private static final long serialVersionUID = -4794606189613680298L;
    @Getter
    private HttpStatus httpStatus;

    public CustomException(String message) {
        this(message, HttpStatus.BAD_REQUEST);
    }

    public CustomException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }
}
