package ca.eazey.notifications.exceptions;

public class UnableToPushNotificationException extends CustomException  {

    private static final long serialVersionUID = 1353085054209276391L;
    public UnableToPushNotificationException(String message) {
        super(message);
    }
}
