package ca.eazey.notifications.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class TopicSubscriptionException extends CustomException {
    private static final long serialVersionUID = 1353085054209276391L;

    public TopicSubscriptionException(String message) {
        super(message);
    }
}
