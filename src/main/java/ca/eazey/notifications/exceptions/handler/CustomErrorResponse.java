package ca.eazey.notifications.exceptions.handler;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.time.LocalDateTime;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class CustomErrorResponse {

    private LocalDateTime timestamp;
    private Integer status;
    private String error;
    private String message;

}
