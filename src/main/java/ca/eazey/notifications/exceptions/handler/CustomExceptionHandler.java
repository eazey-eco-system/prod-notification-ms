package ca.eazey.notifications.exceptions.handler;

import ca.eazey.notifications.exceptions.CustomException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;

@ControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(CustomException.class)
    public final ResponseEntity<CustomErrorResponse> handleException(CustomException ex, WebRequest webRequest) {
        CustomErrorResponse customErrorResponse = new CustomErrorResponse();
        customErrorResponse.setError(ex.getHttpStatus().getReasonPhrase());
        customErrorResponse.setMessage(ex.getMessage());
        customErrorResponse.setTimestamp(LocalDateTime.now());
        customErrorResponse.setStatus(ex.getHttpStatus().value());

        return new ResponseEntity<>(customErrorResponse, ex.getHttpStatus());
    }
}
