package ca.eazey.notifications.model;

public enum GroupPushType {
    TOPIC,
    BATCH
}
