package ca.eazey.notifications.model.firebase;

import lombok.Data;

@Data
public class BatchPushResponse {
    private Integer successCount;
    private Integer failureCount;
    private Integer totalDevices;

    public Integer getTotalDevices() {
        return successCount + failureCount;
    }
}
