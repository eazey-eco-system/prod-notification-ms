package ca.eazey.notifications.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class GroupNotificationResponse {
    @JsonProperty("sent_to_topic")
    private String sentToTopic;

    @JsonProperty("total_devices")
    private Integer totalDevices;

    @JsonProperty("total_success")
    private Integer totalSuccess;

    @JsonProperty("total_failure")
    private Integer totalFailure;
}
