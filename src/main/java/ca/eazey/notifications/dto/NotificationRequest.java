package ca.eazey.notifications.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class NotificationRequest {
    private String title;
    private String message;
    private String token;
    private String deviceId;
    private Map<String, String> data;
}
