package ca.eazey.notifications.dto;

import ca.eazey.notifications.model.GroupPushType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class GroupNotificationRequest {
	private String title;
	private String message;
	List<String> deviceIds;
	private Map<String, String> data;

	@JsonProperty("push_type")
	private GroupPushType pushType;
}
