package ca.eazey.notifications.services;

import ca.eazey.notifications.dto.NotificationRequest;
import ca.eazey.notifications.exceptions.ResourceNotFoundException;
import ca.eazey.notifications.exceptions.UnableToPushNotificationException;

public interface IUserService {
	void sendPushNotification(String userId, NotificationRequest request)
			throws ResourceNotFoundException, UnableToPushNotificationException;
}
