package ca.eazey.notifications.services;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.springframework.stereotype.Service;

import com.google.firebase.messaging.FirebaseMessagingException;

import ca.eazey.notifications.dto.GroupNotificationRequest;
import ca.eazey.notifications.dto.GroupNotificationResponse;
import ca.eazey.notifications.exceptions.ResourceNotFoundException;
import ca.eazey.notifications.exceptions.UnableToPushNotificationException;
import ca.eazey.notifications.mappers.GroupMapper;
import ca.eazey.notifications.model.GroupPushType;
import ca.eazey.notifications.model.firebase.BatchPushResponse;
import ca.eazey.notifications.model.firebase.PushNotificationRequest;
import ca.eazey.notifications.services.firebase.FirebaseMessagingService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class GroupService implements IGroupService {

    private final FirebaseMessagingService firebaseMessagingService;
    private final GroupMapper groupMapper;

    public GroupService(FirebaseMessagingService firebaseMessagingService, GroupMapper groupMapper) {
        this.firebaseMessagingService = firebaseMessagingService;
        this.groupMapper = groupMapper;
    }

    @Override
    public GroupNotificationResponse sendPushNotification(String groupId, GroupNotificationRequest request)
            throws ResourceNotFoundException, UnableToPushNotificationException {
        log.info("Pushing Notification to group: " + groupId);
        log.info("Request Details: " + request);
        if(request == null) {
            throw new UnableToPushNotificationException ("Push Notification Request details missing");
        }

        GroupPushType pushType = request.getPushType();
        if(pushType == null) {
            log.info("No PushType specified, and no default push type configured for group, using TOPIC as default..");
            pushType = GroupPushType.TOPIC;
        }

        if(pushType.equals(GroupPushType.BATCH)) {
            // Push Notification to all group members as BATCH request
            log.info("Pushing notification to total of " + request.getDeviceIds().size() + " users");
            return sendBatchPushNotification(request);
		} else {
			// Push Notification to topic
			log.info("Pushing notification to topic: " + groupId);
			PushNotificationRequest pushNotificationRequest = new PushNotificationRequest(request.getTitle(),
					request.getMessage(), groupId);
			try {
				firebaseMessagingService.sendMessageToTopic(pushNotificationRequest, request.getData());
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
				throw new UnableToPushNotificationException(
						"Unable to push notification to topic " + groupId + ". Error: " + e.getMessage());
			}
			GroupNotificationResponse groupNotificationResponse = new GroupNotificationResponse();
			groupNotificationResponse.setSentToTopic(groupId);
			return groupNotificationResponse;
		}
    }

    private GroupNotificationResponse sendBatchPushNotification(GroupNotificationRequest request)
            throws UnableToPushNotificationException {
        PushNotificationRequest pushNotificationRequest = new PushNotificationRequest(request.getTitle(),
                request.getMessage(),null);
        List<String> deviceIds = request.getDeviceIds();
        if(deviceIds.size() == 0) {
            throw new UnableToPushNotificationException("No devices found to push notification");
        }
        try {
            BatchPushResponse batchPushResponse = firebaseMessagingService.sendBatchMessage(pushNotificationRequest, deviceIds, request.getData());
            log.info("Batch Push response: " + batchPushResponse);
            return groupMapper.modelToDto(batchPushResponse);
        } catch (InterruptedException | ExecutionException | FirebaseMessagingException e) {
            e.printStackTrace();
            throw new UnableToPushNotificationException("Unable to push batch notification. Error: " + e.getMessage());
        }
    }

}
