package ca.eazey.notifications.services.firebase;


public enum NotificationParameter {
    SOUND("default"),
    COLOR("#3CE1A3");

    private String value;

    NotificationParameter(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
