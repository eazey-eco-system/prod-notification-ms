package ca.eazey.notifications.services.firebase;

import ca.eazey.notifications.model.firebase.PushNotificationRequest;
import com.google.firebase.messaging.*;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Component
public class FirebaseMessagingHelper {

    public String sendAndGetResponse(Message message) throws InterruptedException, ExecutionException {
        return FirebaseMessaging.getInstance().sendAsync(message).get();
    }
    public BatchResponse sendBatchAndGetResponse(MulticastMessage message)
            throws InterruptedException, ExecutionException, FirebaseMessagingException {
        return FirebaseMessaging.getInstance().sendMulticast(message);
    }

    public MulticastMessage getPreconfiguredMultiMessage(PushNotificationRequest request,
                                                         List<String> deviceIds,
                                                         Map<String, String> data) {
        return MulticastMessage.builder()
                .setNotification(new Notification(request.getTitle(), request.getMessage()))
                .putAllData(data)
                .addAllTokens(deviceIds)
                .build();
    }

    public Message getPreconfiguredMessageToTopic(PushNotificationRequest request, Map<String, String> data) {
        return getPreconfiguredMessageBuilder(request)
                .putAllData(data)
                .setTopic(request.getTopic())
                .build();
    }

    public Message getPreconfiguredMessageToToken(PushNotificationRequest request, Map<String,String> data) {
        return getPreconfiguredMessageBuilder(request)
                .setToken(request.getToken())
                .putAllData(data)
                .build();
    }

    private Message.Builder getPreconfiguredMessageBuilder(PushNotificationRequest request) {
        AndroidConfig androidConfig = getAndroidConfig(request.getTopic());
        ApnsConfig apnsConfig = getApnsConfig(request.getTopic());
        return Message.builder()
                .setApnsConfig(apnsConfig).setAndroidConfig(androidConfig).setNotification(
                        new Notification(request.getTitle(), request.getMessage()));
    }

    private ApnsConfig getApnsConfig(String topic) {
        return ApnsConfig.builder()
                .setAps(Aps.builder().setCategory(topic).setThreadId(topic).build()).build();
    }

    private AndroidConfig getAndroidConfig(String topic) {
        return AndroidConfig.builder()
                .setTtl(Duration.ofMinutes(2).toMillis()).setCollapseKey(topic)
                .setPriority(AndroidConfig.Priority.HIGH)
                .setNotification(AndroidNotification.builder().setSound("default").setTag(topic).build()).build();
    }
}
