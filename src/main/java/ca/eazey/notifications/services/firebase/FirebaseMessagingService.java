package ca.eazey.notifications.services.firebase;

import ca.eazey.notifications.model.firebase.BatchPushResponse;
import ca.eazey.notifications.model.firebase.PushNotificationRequest;
import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.MulticastMessage;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Service
@Data
public class FirebaseMessagingService {
    private Logger logger = LoggerFactory.getLogger(FirebaseMessagingService.class);

    @Autowired
    FirebaseMessagingHelper firebaseMessagingHelper;

    /**
     * Send message to one device
     * token = the user's firebase instance Id (request.token)
     * data (Optional) = key-value data to pass as part of notification
     * @param request
     * @throws InterruptedException
     * @throws ExecutionException
     */
    public void sendMessage(PushNotificationRequest request, Map<String,String> data)
            throws InterruptedException, ExecutionException {
        if(data == null) data = new HashMap<>();
        Message message = firebaseMessagingHelper.getPreconfiguredMessageToToken(request, data);
        String response = firebaseMessagingHelper.sendAndGetResponse(message);
        logger.info("Sent message to device "+request.getToken()+". Response: " + response);
    }

    /**
     * Send Push notification for batch of devices
     * @param request
     * @param deviceIds
     * @param data
     * @throws InterruptedException
     * @throws ExecutionException
     * @throws FirebaseMessagingException
     */
    public BatchPushResponse sendBatchMessage(PushNotificationRequest request, List<String> deviceIds, Map<String, String> data)
            throws InterruptedException, ExecutionException, FirebaseMessagingException {
        if(data == null) data = new HashMap<>();
        MulticastMessage message = firebaseMessagingHelper.getPreconfiguredMultiMessage(request, deviceIds, data);
        BatchResponse response = firebaseMessagingHelper.sendBatchAndGetResponse(message);
        logger.info("Sent batch message to "+deviceIds.size()+" devices. Response: " + response);

        BatchPushResponse batchPushResponse = new BatchPushResponse();
        batchPushResponse.setFailureCount(response.getFailureCount());
        batchPushResponse.setSuccessCount(response.getSuccessCount());
        return batchPushResponse;
    }

    /**
     * Send message to a Topic
     * topic = topic name to send request to
     * data (Optiona) = key-value data to pass as part of notification
     * @param request
     * @param data
     * @throws InterruptedException
     * @throws ExecutionException
     */
    public void sendMessageToTopic(PushNotificationRequest request, Map<String,String> data)
            throws InterruptedException, ExecutionException {
        if(data == null) data = new HashMap<>();
        Message message = firebaseMessagingHelper.getPreconfiguredMessageToTopic(request, data);
        String response = firebaseMessagingHelper.sendAndGetResponse(message);
        logger.info("Sent message to topic " + request.getTopic() + ". Response: " + response);
    }

}
