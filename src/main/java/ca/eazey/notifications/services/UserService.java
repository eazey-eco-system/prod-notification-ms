package ca.eazey.notifications.services;

import java.util.concurrent.ExecutionException;

import org.springframework.stereotype.Service;

import ca.eazey.notifications.dto.NotificationRequest;
import ca.eazey.notifications.exceptions.ResourceNotFoundException;
import ca.eazey.notifications.exceptions.UnableToPushNotificationException;
import ca.eazey.notifications.model.firebase.PushNotificationRequest;
import ca.eazey.notifications.services.firebase.FirebaseMessagingService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserService implements IUserService {

	private FirebaseMessagingService firebaseMessagingService;

	public UserService(FirebaseMessagingService firebaseMessagingService) {
		this.firebaseMessagingService = firebaseMessagingService;
	}

	@Override
	public void sendPushNotification(String userId, NotificationRequest request)
			throws UnableToPushNotificationException, ResourceNotFoundException {
		if (request == null) {
			throw new UnableToPushNotificationException("Push Notification Request details missing");
		}

		String deviceId = request.getDeviceId();

		log.info("Sending Push Notification to device: " + deviceId);
		PushNotificationRequest pushNotificationRequest = new PushNotificationRequest(request.getTitle(),
				request.getMessage(), null);
		pushNotificationRequest.setToken(request.getToken());
		try {
			firebaseMessagingService.sendMessage(pushNotificationRequest, request.getData());
		} catch (InterruptedException | ExecutionException e) {
			log.error(e.getMessage());
			throw new UnableToPushNotificationException(
					"Unable to push notification to user " + userId + ". Error: " + e.getMessage());
		}
	}
}
