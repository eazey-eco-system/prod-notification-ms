package ca.eazey.notifications.services;

import ca.eazey.notifications.dto.GroupNotificationRequest;
import ca.eazey.notifications.dto.GroupNotificationResponse;
import ca.eazey.notifications.exceptions.ResourceNotFoundException;
import ca.eazey.notifications.exceptions.UnableToPushNotificationException;

public interface IGroupService {

	GroupNotificationResponse sendPushNotification(String groupId, GroupNotificationRequest request)
			throws ResourceNotFoundException, UnableToPushNotificationException;
}
