package ca.eazey.notifications.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ca.eazey.notifications.dto.NotificationRequest;
import ca.eazey.notifications.exceptions.ResourceNotFoundException;
import ca.eazey.notifications.exceptions.UnableToPushNotificationException;
import ca.eazey.notifications.services.IUserService;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/v1/users")
@Slf4j
public class UsersController {

	private final IUserService userService;

	public UsersController(IUserService userService) {
		this.userService = userService;
	}

	@PostMapping("/{user_id}/notify")
	public Map<String, Object> pushNotificationToUser(@PathVariable("user_id") String userId,
			@RequestBody NotificationRequest request)
			throws UnableToPushNotificationException, ResourceNotFoundException {
		log.info("Pushing Notification to user: " + userId);
		log.info("Request Body: " + request);
		Map<String, Object> response = new HashMap<>();

		userService.sendPushNotification(userId, request);
		response.put("result", "success");
		return response;
	}
}
