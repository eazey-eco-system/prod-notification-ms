package ca.eazey.notifications.controllers;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ca.eazey.notifications.dto.GroupNotificationRequest;
import ca.eazey.notifications.dto.GroupNotificationResponse;
import ca.eazey.notifications.exceptions.ResourceNotFoundException;
import ca.eazey.notifications.exceptions.UnableToPushNotificationException;
import ca.eazey.notifications.services.IGroupService;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/v1/groups")
@Slf4j
public class GroupsController {

	private final IGroupService groupService;

	public GroupsController(IGroupService groupService) {
		this.groupService = groupService;
	}

	@PostMapping("/{group_id}/notify")
	public GroupNotificationResponse pushNotificationToUser(@PathVariable("group_id") String groupId,
			@RequestBody GroupNotificationRequest groupNotificationRequest)
			throws UnableToPushNotificationException, ResourceNotFoundException {
		log.info("Group Notification request for group {},  {}", groupId, groupNotificationRequest);
		return groupService.sendPushNotification(groupId, groupNotificationRequest);
	}
}