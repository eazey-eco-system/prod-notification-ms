package ca.eazey.notifications.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/health")
public class HealthCheckController {

	@GetMapping
	public Map<String, Object> healthCheck() {
		Map<String, Object> response = new HashMap<>();
		response.put("status", "UP");
		return response;
	}
}
